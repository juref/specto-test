<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Domain\Post;

class PostTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRoot()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    public function testApiAllPosts()
    {
        $this->get('/api/posts')
            ->seeJsonStructure([
                '*' => [
                    'title',
                    'description',
                    'data',
                ]
            ]);
    }

    public function testApiSinglePost()
    {
        $array = Post::all();
        foreach($array as $data) {
            $id = $data->id;
                // return json_encode($data);
            $this->get('/api/posts/'.$id)
                ->seeJsonStructure([
                    'title',
                    'description',
                    'data',
                ]);
        };
    }
}
