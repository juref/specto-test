<?php

namespace App\Application;

use App\Domain\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class PostController extends Controller
{
    private $redisTTL;

    public function __construct() {
        $this->redisTTL = env('REDIS_TTL')*60;
    }

    // API posts //
    /**
     * @OA\Get(
     *     path="/api/posts",
     *     operationId="/sample/category/things",
     *     tags={"Get all posts"},
     *     @OA\Response(
     *         response="200",
     *         description="Returns all posts",
     *         @OA\JsonContent(),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. Route not set.",
     *     ),
     * )
     */
    public function AllPosts()
    {
        if (Redis::get('posts.api.all')) {
            return Redis::get('posts.api.all');
        }

        Redis::setex('posts.api.all', $this->redisTTL, Post::all());

        return Redis::get('posts.api.all');
    }

    /**
     * @OA\Get(
     *     path="/api/posts/{id}",
     *     operationId="/sample/category/things",
     *     tags={"Get single post"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The id of single post",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Returns single post matching id",
     *         @OA\JsonContent(
     *          @OA\Property(property="id",type="bigIncrements",example="1"),
     *          @OA\Property(property="title",type="string",example="Te\u010daj s chef kuharjem"),
     *          @OA\Property(property="Description",type="string",example="Lorem Ipsum is simply dummy text of the printing and typesetting industry."),
     *          @OA\Property(property="sort",type="tinyInteger",example="1"),
     *          @OA\Property(property="data",type="string",example="{'image': 'posts/post1.jpg', 'video': 'posts/video1.mp4'}"),
     *          @OA\Property(property="status",type="boolean",example="false"),
     *          @OA\Property(property="crated_at",type="timestamps",example="null"),
     *          @OA\Property(property="updated_at",type="timestamps",example="null"),
     *          @OA\Property(property="deleted_at",type="timestamps",example="null"),
     *         )
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Error: Bad request. Route not set.",
     *     ),
     * )
     */
    public function SinglePost($id)
    {
        if (Redis::get('posts.api.all')) {
            return json_decode(Redis::get('posts.api.all'), true)[$id - 1];
        }

        Redis::setex('posts.api.all', $this->redisTTL, Post::all());

        return json_decode(Redis::get('posts.api.all'), true)[$id - 1];
    }

    // View posts //
    public function showAllposts()
    {
        if (Redis::get('posts.api.all')) {
            return view('posts', ['posts' => json_decode(Redis::get('posts.api.all'))]);
        }

        Redis::setex('posts.api.all', $this->redisTTL, Post::all());

        return view('posts', ['posts' => json_decode(Redis::get('posts.api.all'))]);
    }

    public function showSinglePost($id)
    {
        if (Redis::get('posts.api.all')) {
            return view('single-post', ['singlePost' => json_decode(Redis::get('posts.api.all'))[$id - 1]]);
        }

        Redis::setex('posts.api.all', $this->redisTTL, Post::all());

        return view('single-post', ['singlePost' => json_decode(Redis::get('posts.api.all'))[$id - 1]]);
    }
}
