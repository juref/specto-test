@extends('layouts.app')

@section('body_class', 'posts')
@section('content')
    @foreach($posts as $post)
    <a href="/show/posts/{{ $post->id }}">
        {{ $post->title }}<br>
    </a>
    @endforeach
@endsection
