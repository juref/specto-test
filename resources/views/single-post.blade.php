@extends('layouts.app')

@section('body_class', 'posts')
@section('content')
    <h1>{{ $singlePost->title }}</h1><br>
    {{ $singlePost->description }}<br><br>
    @isset(json_decode($singlePost->data)->image)
        <img width="320" height="auto" src="/{{ json_decode($singlePost->data)->image }}" alt="{{ json_decode($singlePost->data)->image }}"><br>
    @endisset
    @isset(json_decode($singlePost->data)->video)
        <video width="320" height="auto" src="/{{ json_decode($singlePost->data)->video }}" autoplay loop></video>
    @endisset
@endsection
