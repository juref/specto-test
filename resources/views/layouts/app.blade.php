<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('partials.head')
    @include('partials.header')

    <body class="body @yield('body_class')">
        @yield('content')
        @include('partials.footer')
    </body>
</html>
